This is a sample university management application. 

It allows sorting students, assigning them and teachers to subjects/groups etc.

Unit tests allow to generate random data sample for all classes,
big enough to be able to imitate real-world scenario of a university.
