package psieminski;

import java.util.List;
import java.util.Objects;

public class StudentGroup implements Comparable<StudentGroup> {

    private String name;
    private final List<Student> students;

    public StudentGroup(String name, List<Student> students) {
        if (students.size() > 10) {
            throw new IllegalArgumentException("Maximum number of students is 10, your list contains: " + students.size());
        }
        this.name = name;
        this.students = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentGroup that = (StudentGroup) o;
        return name.equals(that.name) && students.equals(that.students);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, students);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    @Override
    public int compareTo(StudentGroup otherGroup) {
        int value = this.getName().compareTo(otherGroup.getName());
        if (value != 0) {
            return value;
        }
        int firstSize = this.getStudents().size();
        int secondSize = otherGroup.getStudents().size();
        if (firstSize == secondSize) {
            return 0;
        } else if (firstSize > secondSize) {
            return 1;
        }
        return -1;
    }

}
