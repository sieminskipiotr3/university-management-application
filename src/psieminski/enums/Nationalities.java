package psieminski.enums;

public enum Nationalities {

    //custom list of nationalities set; possible extension
    Polish("PL"),
    Ukrainian("UA"),
    Belarussian("BY"),
    Slovak("SK"),
    Lithuanian("LT"),
    Latvian("LV"),
    British("UK"),
    Indian("IN"),
    Chinese("CN"),
    Vietnamese("VN");

    public final String country;

    public String getCountry() {
        return country;
    }

    Nationalities(String country) {
        this.country = country;
    }

}
