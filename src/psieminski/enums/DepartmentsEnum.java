package psieminski.enums;

public enum DepartmentsEnum {

    //brief, example, easy extension possible
    InformationTechnology("IT"),
    Mathematics("Math"),
    HumanSciences("HS"),
    Engineering("ENG"),
    FrancophoneStudies("French History and Literature");

    public final String department;

    public String getDepartment() {
        return department;
    }

    DepartmentsEnum(String department) {
        this.department = department;
    }

}
