package psieminski.enums;

public enum SubjectsNames {

    //brief, example, easy extension possible
    InformationTechnology("Computer Science"),
    Physics("Physics"),
    Mathematics("Math"),
    SocialSciences("Sociology"),
    EngineeringPractival("Building bridges"),
    FrenchLanguageStudies("French");

    public final String subject;

    public String getSubject() {
        return subject;
    }

    SubjectsNames(String subject) {
        this.subject = subject;
    }

}
