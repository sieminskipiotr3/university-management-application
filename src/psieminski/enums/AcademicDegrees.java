package psieminski.enums;

public enum AcademicDegrees {

    BachelorOfArts("BA"),
    BachelorOfScience("BS"),
    MasterOfArts("MA"),
    MasterOfScience("MS"),
    DoctorOfPhilosophy("PhD");

    public final String title;

    public String getTitle() {
        return title;
    }

    AcademicDegrees(String title) {
        this.title = title;
    }

}
