package psieminski;

import java.util.List;
import java.util.Objects;

public class Subject {

    private String name;
    private Department department;
    private Teacher lecturer;
    private List<Student> students;

    public Subject(String name, Department department, Teacher lecturer, List<Student> students) {
        this.name = name;
        this.department = department;
        this.lecturer = lecturer;
        this.students = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subject subject = (Subject) o;
        return name.equals(subject.name) && department.equals(subject.department) && lecturer.equals(subject.lecturer) && students.equals(subject.students);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, department, lecturer, students);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Teacher getLecturer() {
        return lecturer;
    }

    public void setLecturer(Teacher lecturer) {
        this.lecturer = lecturer;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
