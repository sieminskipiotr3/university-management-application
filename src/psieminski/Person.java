package psieminski;

import psieminski.validators.PeselValidator;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Objects;

public class Person implements Comparable<Person> {

    private final String pesel; //polish version of social security number
    private final String firstName;
    private final String lastName;
    private final LocalDate birthDate;
    private final Locale nationality;

    public Person(String pesel, String firstName, String lastName, LocalDate birthDate, Locale nationality) {
        PeselValidator validator = new PeselValidator(pesel);
        if (!validator.validatePesel()) {
            throw new IllegalArgumentException("This PESEL is not correct");
        }
        this.pesel = pesel;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.nationality = nationality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return pesel.equals(person.pesel) && firstName.equals(person.firstName) && lastName.equals(person.lastName) && birthDate.equals(person.birthDate) && nationality.equals(person.nationality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pesel, firstName, lastName, birthDate, nationality);
    }

    public String getPesel() {
        return pesel;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Locale getNationality() {
        return nationality;
    }

    public String getCountry() {
        return nationality.getCountry();
    }

    @Override
    public int compareTo(Person otherPerson) {
        int value = this.getLastName().compareTo(otherPerson.getLastName());
        if (value != 0) {
            return value;
        }
        value = this.getFirstName().compareTo(otherPerson.getFirstName());
        if (value != 0) {
            return value;
        }
        value = this.getBirthDate().compareTo(otherPerson.getBirthDate());
        return value;
    }

}
