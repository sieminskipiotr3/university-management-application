package psieminski.tests;

import org.junit.jupiter.api.Test;
import psieminski.Student;
import psieminski.enums.Nationalities;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    Student student1 = new Student("12345678998", "First", "Last", LocalDate.parse("2000-02-08"), new Locale("en", Nationalities.Polish.getCountry()), BigInteger.valueOf(1000000));
    Student student2 = new Student("12345678998", "First", "Last", LocalDate.parse("2000-02-08"), new Locale("en", Nationalities.Polish.getCountry()), BigInteger.valueOf(1000000));


    @Test
    void testEquals() {
        assertTrue(student1.equals(student2));
    }

    @Test
    void testCompareTo() {
        assertEquals(0, student1.compareTo(student2));
    }

}