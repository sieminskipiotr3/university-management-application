package psieminski.tests;

import org.junit.jupiter.api.Test;
import psieminski.RandomDataGenerator;
import psieminski.collections.Departments;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentsTest {

    RandomDataGenerator random = new RandomDataGenerator(200, 10, 20, 3, 10);

    @Test
    void testForDisallowingDuplicates() {

        var randomDepartments = random.getDepartments().getDepartments();
        var d1 = randomDepartments.get(0);
        var d2 = d1;
        randomDepartments.add(d1);
        randomDepartments.add(d2);
        var departments = new Departments(randomDepartments);

        assertEquals(3, departments.getDepartments().size());

    }
}