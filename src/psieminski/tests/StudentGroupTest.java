package psieminski.tests;

import org.junit.jupiter.api.Test;
import psieminski.RandomDataGenerator;
import psieminski.Student;
import psieminski.StudentGroup;
import psieminski.enums.Nationalities;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class StudentGroupTest {

    RandomDataGenerator random = new RandomDataGenerator(200, 10, 20, 3, 10);

    @Test
    void illegalThrowTestGroups() {
        List<Student> list1 = random.getStudentGroups().get(0).getStudents();
        if (list1.size() < 10) {
            list1 = random.getStudentGroups().get(0).getStudents();
        }
        Student s = new Student("12345678998", "First", "Last", LocalDate.parse("2000-02-08"), new Locale("en", Nationalities.Polish.getCountry()), BigInteger.valueOf(1000000));
        list1.add(s);
        final List<Student> checkList = list1;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new StudentGroup("RandomName", checkList);
        });

        String expected = "Maximum number of students is 10, your list contains: 11";
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }
}