package psieminski.tests;

import org.junit.jupiter.api.Test;
import psieminski.Teacher;
import psieminski.enums.AcademicDegrees;
import psieminski.enums.Nationalities;

import java.time.LocalDate;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class TeacherTest {

    Teacher teacher1 = new Teacher("12345678998", "first", "last", LocalDate.parse("1958-05-09"), new Locale("en", Nationalities.British.getCountry()), AcademicDegrees.MasterOfArts, LocalDate.parse("2015-06-09"));
    Teacher teacher2 = new Teacher("12345678998", "first", "last", LocalDate.parse("1958-05-09"), new Locale("en", Nationalities.British.getCountry()), AcademicDegrees.MasterOfArts, LocalDate.parse("2015-06-09"));

    @Test
    void testEquals() {
        assertTrue(teacher1.equals(teacher2));
    }

    @Test
    void testHashCode() {
        assertEquals(teacher1.hashCode(), teacher2.hashCode());
    }
}