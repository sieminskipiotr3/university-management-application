package psieminski.tests;

import org.junit.jupiter.api.Test;
import psieminski.Person;
import psieminski.RandomDataGenerator;
import psieminski.Student;
import psieminski.collections.Students;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class StudentsTest {

    RandomDataGenerator random = new RandomDataGenerator(200, 10, 20, 3, 10);

    @Test
    void getByPolishCollationList() {
        List<Student> students = random.getStudents().getByPolishCollationList();
        students.sort(Person::compareTo);
        String lastNameCheck = students.get(0).getLastName();
        assertEquals(lastNameCheck, students.get(0).getLastName());
    }

    @Test
    void getByNationality() {
        Students students = random.getStudents();
        List<Student> checkList = students.getByNationality("UK");
        List<Student> expected = students.getStudents()
                .stream()
                .filter(x -> x.getNationality().getCountry().equals("UK"))
                .collect(Collectors.toList());
        expected.sort(Comparator.comparing(Student::getLastName));
        assertEquals(expected, checkList);
    }
}