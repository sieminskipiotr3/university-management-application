package psieminski.collections;

import psieminski.Person;
import psieminski.Student;
import psieminski.Teacher;
import psieminski.comparators.PolishCollationComparator;

import java.text.Collator;
import java.util.*;
import java.util.stream.Collectors;

public class Teachers {

    private final List<Teacher> teachers;
    private static final Comparator<Person> byPolishCollation = new PolishCollationComparator();
    private final List<Teacher> byPolishCollationList;
    private final Map<String, List<Teacher>> filterByNationalitySortByName;

    public Teachers(List<Teacher> teachers) {
        List<Teacher> unique = new ArrayList<>();
        for (Teacher t : teachers) {
            if (unique.contains(t)) {
                continue;
            } else {
                unique.add(t);
            }
        }
        this.teachers = unique;

        byPolishCollationList = new ArrayList<>(unique);
        byPolishCollationList.sort(byPolishCollation);

        filterByNationalitySortByName = unique
                .stream()
                .collect(Collectors.groupingBy(Person::getCountry, TreeMap::new, Collectors.mapping(x -> x, Collectors.toList())));
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public static Comparator<Person> getByPolishCollation() {
        return byPolishCollation;
    }

    public List<Teacher> getByPolishCollationList() {
        return byPolishCollationList;
    }

    //sorts teachers based on selected collation rules
    //the same function as for students
    public List<Teacher> getByNationality(String nationality) {
        Locale locale = new Locale("en", nationality);
        Collator collator = Collator.getInstance(locale);
        List<Teacher> sorted = filterByNationalitySortByName.get(nationality);
        sorted.sort((x, y) -> collator.compare(x.getLastName(), y.getLastName()));
        return sorted;
    }

}
