package psieminski.collections;

import psieminski.Person;
import psieminski.Student;
import psieminski.comparators.PolishCollationComparator;

import java.text.Collator;
import java.util.*;
import java.util.stream.Collectors;

public class Students {

    private final List<Student> students;
    private static final Comparator<Person> byPolishCollation = new PolishCollationComparator();
    private final List<Student> byPolishCollationList;
    public Map<String, List<Student>> filterByNationalitySortByName;

    public Students(List<Student> students) {

        List<Student> unique = new ArrayList<>();
        for (Student s : students) {
            if (unique.contains(s)) {
                continue;
            } else {
                unique.add(s);
            }
        }
        this.students = unique;

        byPolishCollationList = new ArrayList<>(unique);
        byPolishCollationList.sort(byPolishCollation);

        filterByNationalitySortByName = unique
                .stream()
                .collect(Collectors.groupingBy(Person::getCountry, TreeMap::new, Collectors.mapping(x -> x, Collectors.toList())));
    }

    public List<Student> getStudents() {
        return students;
    }

    public static Comparator<Person> getByPolishCollation() {
        return byPolishCollation;
    }

    public List<Student> getByPolishCollationList() {
        return byPolishCollationList;
    }

    //sorts students last names based on selected collation rules
    public List<Student> getByNationality(String nationality) {
        Locale locale = new Locale("en", nationality);
        Collator collator = Collator.getInstance(locale);
        List<Student> sorted = filterByNationalitySortByName.get(nationality);
        sorted.sort((x, y) -> collator.compare(x.getLastName(), y.getLastName()));
        return sorted;
    }
}
