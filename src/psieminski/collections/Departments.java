package psieminski.collections;

import psieminski.Department;
import psieminski.comparators.DepartmentComparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Departments {

    private final List<Department> departments;
    private final List<Department> sortedDepartmentsByName;
    private static final Comparator<Department> compareDepartments = new DepartmentComparator();

    public Departments(List<Department> departments) {
        List<Department> unique = new ArrayList<>();
        for (Department d : departments) {
            if (unique.contains(d)) {
                continue;
            } else {
                unique.add(d);
            }
        }
        this.departments = unique;

        sortedDepartmentsByName = new ArrayList<>(unique);
        sortedDepartmentsByName.sort(compareDepartments);
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public List<Department> getSortedDepartmentsByName() {
        return sortedDepartmentsByName;
    }
}
