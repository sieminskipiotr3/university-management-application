package psieminski.collections;


import psieminski.Subject;
import psieminski.comparators.SubjectComparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Subjects {

    private final List<Subject> subjects;
    private final List<Subject> sortedSubjects;
    private static final Comparator<Subject> compareSubjects = new SubjectComparator();

    public Subjects(List<Subject> subjects) {
        List<Subject> unique = new ArrayList<>();
        for (Subject s : subjects) {
            if (unique.contains(s)) {
                continue;
            } else {
                unique.add(s);
            }
        }
        this.subjects = unique;

        sortedSubjects = new ArrayList<>(unique);
        sortedSubjects.sort(compareSubjects);
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public List<Subject> getSortedSubjects() {
        return sortedSubjects;
    }
}
