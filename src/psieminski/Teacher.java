package psieminski;

import psieminski.enums.AcademicDegrees;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Objects;

public class Teacher extends Person {

    private final AcademicDegrees degree;
    private final LocalDate hireDate;


    public Teacher(String pesel, String firstName, String lastName, LocalDate birthDate, Locale nationality, AcademicDegrees degree, LocalDate hireDate) {
        super(pesel, firstName, lastName, birthDate, nationality);
        this.degree = degree;
        this.hireDate = hireDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Teacher teacher = (Teacher) o;
        return degree == teacher.degree && hireDate.equals(teacher.hireDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), degree, hireDate);
    }

    public String getDegree() {
        return degree.getTitle();
    }

    public LocalDate getHireDate() {
        return hireDate;
    }

}
