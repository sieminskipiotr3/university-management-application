package psieminski;

import java.util.List;
import java.util.Objects;

public class Department {

    private String name;
    private List<Teacher> teacherList;

    public Department(String name, List<Teacher> teacherList) {
        this.name = name;
        this.teacherList = teacherList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return name.equals(that.name) && teacherList.equals(that.teacherList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, teacherList);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Teacher> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<Teacher> teacherList) {
        this.teacherList = teacherList;
    }

    public int compareDepartments(Department otherDepartment) {
        int value = this.getName().compareTo(otherDepartment.getName());
        return value;
    }

}
