package psieminski.validators;

import java.util.regex.Pattern;

public class PeselValidator {

    //polish social security number (PESEL) consists of 11 digits
    private static final String pattern = "\\d{11}";
    private static final Pattern regex = Pattern.compile(pattern);
    private final String pesel;

    public PeselValidator(String pesel) {
        this.pesel = pesel;
    }

    public Boolean validatePesel() {
        return regex.matcher(this.pesel).matches();
    }

}
