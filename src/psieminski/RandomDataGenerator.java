package psieminski;

import psieminski.collections.Departments;
import psieminski.collections.Students;
import psieminski.collections.Subjects;
import psieminski.collections.Teachers;
import psieminski.enums.AcademicDegrees;
import psieminski.enums.DepartmentsEnum;
import psieminski.enums.Nationalities;
import psieminski.enums.SubjectsNames;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class RandomDataGenerator {

    private final Students students;
    private final List<StudentGroup> studentGroups;
    private final Departments departments;
    private final Subjects subjects;
    private final Teachers teachers;

    private static final Random rand = new Random();

    private static final String[] Beginning = {"Kr", "Ca", "Ra", "Mrok", "Cru",
            "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar", "Mjol",
            "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro",
            "Mar", "Luk"};
    private static final String[] Middle = {"air", "ir", "mi", "sor", "mee", "clo",
            "red", "cra", "ark", "arc", "miri", "lori", "cres", "mur", "zer",
            "marac", "zoir", "slamar", "salmar", "urak"};
    private static final String[] End = {"d", "ed", "ark", "arc", "es", "er", "der",
            "tron", "med", "ure", "zur", "cred", "mur"};

    private static final List<Nationalities> valuesNationalities = Collections.unmodifiableList(Arrays.asList(Nationalities.values()));
    private static final int sizeNationalities = valuesNationalities.size();

    private static final List<AcademicDegrees> valuesDegrees = Collections.unmodifiableList(Arrays.asList(AcademicDegrees.values()));
    private static final int sizeDegrees = valuesDegrees.size();

    private static final List<DepartmentsEnum> valuesDepartments = Collections.unmodifiableList(Arrays.asList(DepartmentsEnum.values()));
    private static final int sizeDepartments = valuesDepartments.size();

    private static final List<SubjectsNames> valuesSubjects = Collections.unmodifiableList(Arrays.asList(SubjectsNames.values()));
    private static final int sizeSubjects = valuesSubjects.size();

    BigInteger maxLimit = new BigInteger("9999999");
    int BigIntegerLen = maxLimit.bitLength();

    long minDay = LocalDate.of(1950, 1, 1).toEpochDay();
    long maxDay = LocalDate.of(2000, 12, 31).toEpochDay();
    long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);

    public RandomDataGenerator(int students, int teachers, int groups, int departments, int subjects) {
        if (groups % 10 != 0) {
            throw new IllegalArgumentException("Number of groups must be 10 times less than students");
        }
        if (departments % 3 != 0) {
            throw new IllegalArgumentException("Number of departments must be divisible by 3");
        }
        if (teachers != subjects) {
            throw new IllegalArgumentException("Number of teachers and subjects must be the same");
        }
        this.students = generateStudents(students);
        this.teachers = generateTeachers(teachers);
        this.studentGroups = generateStudentGroups(groups);
        this.departments = generateDepartments(departments);
        this.subjects = generateSubjects(subjects);
    }


    public Students getStudents() {
        return students;
    }

    public Teachers getTeachers() {
        return teachers;
    }

    public Departments getDepartments() {
        return departments;
    }

    public Subjects getSubjects() {
        return subjects;
    }

    public List<StudentGroup> getStudentGroups() {
        return studentGroups;
    }

    public static String generateName() {
        return Beginning[rand.nextInt(Beginning.length)] +
                Middle[rand.nextInt(Middle.length)] +
                End[rand.nextInt(End.length)];
    }

    public String generateRandomPesel() {
        return BigInteger.probablePrime(50, new Random()).toString().substring(0, 11);
    }

    public LocalDate generateRandomDate() {
        return LocalDate.ofEpochDay(randomDay);
    }

    public Locale generateRandomLocale() {
        return new Locale("en", valuesNationalities.get(rand.nextInt(sizeNationalities)).country);
    }

    public BigInteger generateRandomBookNumber() {
        return new BigInteger(BigIntegerLen, rand);
    }

    public AcademicDegrees generateRandomDegree() {
        return valuesDegrees.get(rand.nextInt(sizeDegrees));
    }

    public String generateRandomDepartmentName() {
        return valuesDepartments.get(rand.nextInt(sizeDepartments)).getDepartment();
    }

    public String generateRandomSubjectName() {
        return valuesSubjects.get(rand.nextInt(sizeSubjects)).getSubject();
    }

    public Students generateStudents(int numberOfRandomStudents) {
        List<Student> s1 = new ArrayList<>();
        for (int i = 0; i < numberOfRandomStudents; i++) {
            String pesel = generateRandomPesel();
            String name = generateName();
            String lastName = generateName();
            LocalDate date = generateRandomDate();
            Locale randomNationality = generateRandomLocale();
            BigInteger randomBookNumber = generateRandomBookNumber();
            Student s = new Student(pesel, name, lastName, date, randomNationality, randomBookNumber);
            if (s1.contains(s)) {
                continue;
            } else {
                s1.add(s);
            }
        }
        while (s1.size() < numberOfRandomStudents) {
            generateStudents(numberOfRandomStudents);
        }
        return new Students(s1);
    }

    public Teachers generateTeachers(int numberOfTeachers) {
        List<Teacher> t1 = new ArrayList<>();
        for (int i = 0; i < numberOfTeachers; i++) {
            String pesel = generateRandomPesel();
            String name = generateName();
            String lastName = generateName();
            LocalDate date = generateRandomDate();
            LocalDate hireDate = generateRandomDate();
            while (hireDate.isBefore(date)) {
                hireDate = generateRandomDate();
            }
            Locale randomNationality = generateRandomLocale();
            AcademicDegrees degree = generateRandomDegree();
            Teacher t = new Teacher(pesel, name, lastName, date, randomNationality, degree, hireDate);
            if (t1.contains(t)) {
                continue;
            } else {
                t1.add(t);
            }
        }
        while (t1.size() < numberOfTeachers) {
            generateTeachers(numberOfTeachers);
        }
        return new Teachers(t1);
    }

    public List<StudentGroup> generateStudentGroups(int numberOfGroups) {
        List<StudentGroup> groups = new ArrayList<>();
        for (int i = 0; i < numberOfGroups; i++) {
            String name = generateName();
            List<Student> s1 = new ArrayList<>();
            for (int j = 0; j < 10; j++) {
                Student s = students.getStudents().get(rand.nextInt(students.getStudents().size()));
                if (s1.contains(s)) {
                    continue;
                } else {
                    s1.add(s);
                }
            }
            StudentGroup group = new StudentGroup(name, s1);
            if (groups.contains(group)) {
                continue;
            } else {
                groups.add(group);
            }
        }
        while (groups.size() < numberOfGroups) {
            generateStudentGroups(numberOfGroups);
        }
        return groups;
    }

    public Departments generateDepartments(int numberOfDepartments) {
        List<Department> departmentsList = new ArrayList<>();
        for (int i = 0; i < numberOfDepartments; i++) {
            String name = generateRandomDepartmentName();
            List<Teacher> t1 = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                Teacher t = teachers.getTeachers().get(rand.nextInt(teachers.getTeachers().size()));
                if (t1.contains(t)) {
                    continue;
                } else {
                    t1.add(t);
                }
            }
            Department d1 = new Department(name, t1);
            if (departmentsList.contains(d1)) {
                continue;
            } else {
                departmentsList.add(d1);
            }
        }
        while (departmentsList.size() < numberOfDepartments) {
            generateDepartments(numberOfDepartments);
        }
        return new Departments(departmentsList);
    }

    public Subjects generateSubjects(int numberOfSubjects) {
        List<Subject> subjectList = new ArrayList<>();
        for (int i = 0; i < numberOfSubjects; i++) {
            String name = generateRandomSubjectName();
            Department d1 = departments.getDepartments().get(rand.nextInt(departments.getDepartments().size()));
            Teacher t1 = teachers.getTeachers().get(rand.nextInt(teachers.getTeachers().size()));
            List<Student> s1 = studentGroups.get(rand.nextInt(studentGroups.size())).getStudents();
            Subject s = new Subject(name, d1, t1, s1);
            if (subjectList.contains(s)) {
                continue;
            } else {
                subjectList.add(s);
            }
        }
        while (subjectList.size() < numberOfSubjects) {
            generateSubjects(numberOfSubjects);
        }
        return new Subjects(subjectList);
    }

}
