package psieminski;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Locale;
import java.util.Objects;

public class Student extends Person {

    private final BigInteger studentBookNumber;

    public Student(String pesel, String firstName, String lastName, LocalDate birthDate, Locale nationality, BigInteger studentBookNumber) {
        super(pesel, firstName, lastName, birthDate, nationality);
        this.studentBookNumber = studentBookNumber;
    }

    public BigInteger getStudentBookNumber() {
        return studentBookNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return studentBookNumber.equals(student.studentBookNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentBookNumber);
    }
}
