package psieminski.comparators;

import psieminski.Person;
import psieminski.enums.Nationalities;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public final class PolishCollationComparator implements Comparator<Person> {

    //compares based on polish collation
    //custom order of comparisons set
    @Override
    public int compare(Person person1, Person person2) {
        Locale locale = new Locale("en", Nationalities.Polish.getCountry());
        Collator collator = Collator.getInstance(locale);
        int value = collator.compare(person1.getLastName(), person2.getLastName());
        if(value != 0) {
            return value;
        }
        value = collator.compare(person1.getFirstName(), person2.getFirstName());
        if(value != 0) {
            return value;
        }
        value = collator.compare(person1.getBirthDate(), person2.getBirthDate());
        if (value != 0) {
            return value;
        }
        value = collator.compare(person1.getPesel(), person2.getPesel());
        return value;
    }
}
