package psieminski.comparators;

import psieminski.Subject;

import java.util.Comparator;

public class SubjectComparator implements Comparator<Subject> {

    //custom order of natural order comparisons set
    @Override
    public int compare(Subject o1, Subject o2) {
        int value = o1.getDepartment().compareDepartments(o2.getDepartment());
        if (value != 0) {
            return value;
        }
        value = o1.getName().compareTo(o2.getName());
        if (value != 0) {
            return value;
        }
        value = o1.getLecturer().compareTo(o2.getLecturer());
        return value;
    }


}
