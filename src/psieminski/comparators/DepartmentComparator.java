package psieminski.comparators;

import psieminski.Department;

import java.util.Comparator;

public class DepartmentComparator implements Comparator<Department> {

    //comparing natural order by name
    @Override
    public int compare(Department o1, Department o2) {
        return o1.getName().compareTo(o2.getName());
    }

}
